SELECT B.TITLE, CASE WHEN D.TA_PDF IS NULL
					THEN '#' ELSE D.TA_PDF END AS PDF, CASE WHEN D.Journal_URL is null
														then '#' else D.Journal_URL end as URL_J, case when D.Journal_PDF is null
																									then '#' else D.Journal_PDF end as PDF_J

FROM RELATIONSHIP AS A

INNER JOIN ARTICLES AS B
ON B.ID = A.JOURNAL_ID

Inner JOIN AUTHORS AS C
ON C.ID = A.AUTHOR_ID

left JOIN Media as D
on D.Article_ID = A.JOURNAL_ID

where A.JOURNAL_ID = 45

GROUP BY 1
order by 1;
 
